package com.phamcongtuan.model;

import java.io.Serializable;

/**
 * Created by Admin on 9/12/2017.
 */

public class SinhVien implements Serializable{
    private String hoTen;
    private String diaChi;
    private int namSinh;
    private String email;

    public SinhVien(String hoTen, String diaChi, int namSinh, String email) {
        this.hoTen = hoTen;
        this.diaChi = diaChi;
        this.namSinh = namSinh;
        this.email = email;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public int getNamSinh() {
        return namSinh;
    }

    public void setNamSinh(int namSinh) {
        this.namSinh = namSinh;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
