package com.phamcongtuan.fragmentxulygiaodien;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.phamcongtuan.model.SinhVien;

public class MainActivity extends AppCompatActivity implements TruyenSinhVien{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void DataSinhVien(SinhVien sinhVien) {
        FragmentStudentInfo fragmentStudentInfo = (FragmentStudentInfo) getFragmentManager().findFragmentById(R.id.fragmentInfo);

        Configuration configuration = getResources().getConfiguration();

        if (fragmentStudentInfo != null && configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
            fragmentStudentInfo.SetStudentInfo(sinhVien);
        } else {
            Intent intent = new Intent(MainActivity.this, StudentInfoActivity.class);
            intent.putExtra("thongTinSV", sinhVien);
            startActivity(intent);
        }
    }
}
