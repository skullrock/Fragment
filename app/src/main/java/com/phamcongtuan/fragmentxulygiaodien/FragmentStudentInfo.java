package com.phamcongtuan.fragmentxulygiaodien;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.phamcongtuan.model.SinhVien;

/**
 * Created by Admin on 9/12/2017.
 */

public class FragmentStudentInfo extends Fragment {
    TextView txtTen, txtDiaChi, txtNamSinh, txtEmail;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_student_info, container, false);

        addControls();

        return view;
    }

    public void SetStudentInfo(SinhVien sinhVien){
        txtTen.setText(sinhVien.getHoTen());
        txtNamSinh.setText("Nam sinh: " + sinhVien.getNamSinh());
        txtDiaChi.setText("Dia chi: " + sinhVien.getDiaChi());
        txtEmail.setText("Email: " + sinhVien.getEmail());
    }

    private void addControls(){
        txtTen      = (TextView) view.findViewById(R.id.txtTen);
        txtDiaChi   = (TextView) view.findViewById(R.id.txtDiaChi);
        txtNamSinh  = (TextView) view.findViewById(R.id.txtNamSinh);
        txtEmail    = (TextView) view.findViewById(R.id.txtEmail);
    }
}
