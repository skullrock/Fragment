package com.phamcongtuan.fragmentxulygiaodien;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.phamcongtuan.adapter.SinhVienAdapter;
import com.phamcongtuan.model.SinhVien;

import java.util.ArrayList;

/**
 * Created by Admin on 9/12/2017.
 */

public class FragmentStudentList extends ListFragment {

    ArrayList<SinhVien> arrSinhVien;
    SinhVienAdapter adapterSinhVien;

    TruyenSinhVien truyenSinhVien;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        truyenSinhVien = (TruyenSinhVien) getActivity();

        arrSinhVien = new ArrayList<>();
        AddSinhVien();

        adapterSinhVien = new SinhVienAdapter(getActivity(), R.layout.item_sinhvien, arrSinhVien);
        setListAdapter(adapterSinhVien);

        return inflater.inflate(R.layout.fragment_student_list, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        truyenSinhVien.DataSinhVien(arrSinhVien.get(position));
    }

    private void AddSinhVien(){
        arrSinhVien.add(new SinhVien("Nguyen Van A", "TP.HCM", 2000, "nva@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van B", "Ha Noi", 2001, "nvb@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van C", "Hue", 2002, "nvc@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van D", "Hai Phong", 2004, "nvd@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van E", "Long An", 2000, "nve@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van F", "Dong Nai", 2002, "nvf@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van G", "Binh Duong", 2000, "nvg@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van H", "Binh Phuoc", 2005, "nvh@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van I", "Tay Ninh", 2001, "nvi@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van J", "Cao Bang", 2002, "nvj@gmail.com"));
        arrSinhVien.add(new SinhVien("Nguyen Van K", "Thai Binh", 2000, "nvk@gmail.com"));
    }
}
